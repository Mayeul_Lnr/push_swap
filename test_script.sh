#! /bin/bash

for ((i=1; i<=$1; i++)); do
	echo -n "$i/$1 - "
	ARG=$(for ((j=1; j<=$2; j++)) ; do echo $j ; done | sort -R | tr '\n' ' ' | sed 's/ $//')
	./push_swap $ARG | ../checker_size $ARG
done
