/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_parsing.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/26 18:39:48 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 23:24:07 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

static int	split_clear(char **split, int splitsize)
{
	while (splitsize--)
		free(split[splitsize]);
	free(split);
	return (0);
}

static int	storage_fill(char *s, t_prog **prog, int *size)
{
	char	**split;
	int		splitsize;
	int		val;
	int		i;

	split = ft_split(s, ' ', &splitsize);
	if (!split)
		return (-1);
	i = -1;
	while (++i < splitsize)
	{
		if (ft_atoichk(split[i], &val) < 0)
			return (split_clear(split, splitsize) - 1);
		if (*prog && (*prog)->op == val)
			return (split_clear(split, splitsize) - 1);
		if (prog_push(prog, val, 0) < 0)
			return (split_clear(split, splitsize) - 1);
	}
	*size += splitsize;
	return (split_clear(split, splitsize));
}

static t_stack	*prog_to_stack(t_prog *prog, int size)
{
	t_prog	*temp;
	t_stack	*a;

	a = stack_init(size);
	if (!a)
		return (NULL);
	while (prog)
	{
		if (stack_findval(a, prog->op) >= 0)
		{
			free(a->ring);
			free(a);
			return ((t_stack *)prog_clear(&prog));
		}
		stack_push(a, prog->op);
		temp = prog;
		prog = prog->next;
		free(temp);
	}
	return (a);
}

t_stack	*stack_parsing(int ac, char **av)
{
	t_prog	*prog;
	int		size;
	int		i;

	prog = NULL;
	size = 0;
	i = 0;
	while (++i < ac)
		if (storage_fill(av[i], &prog, &size) < 0)
			return ((t_stack *)prog_clear(&prog));
	return (prog_to_stack(prog, size));
}

t_stack	*stack_init(int size)
{
	t_stack	*ret;

	ret = malloc(sizeof(t_stack));
	if (!ret)
		return (NULL);
	ret->ring = malloc((size + 1) * sizeof(int));
	if (!ret->ring)
	{
		free(ret);
		return (NULL);
	}
	ret->max_size = size + 1;
	ret->top = size + 1;
	ret->bot = 0;
	ret->size = 0;
	return (ret);
}
