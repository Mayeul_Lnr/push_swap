/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prog.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/17 18:05:05 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 01:58:31 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

int	prog_push(t_prog **prog, int op, int repeat)
{
	t_prog	*new;

	if (!prog)
		return (-1);
	if (*prog && (*prog)->op == op)
		(*prog)->repeat += repeat;
	else
	{
		new = malloc(sizeof(t_prog));
		if (!new)
			return (-1);
		new->op = op;
		new->repeat = repeat;
		new->next = *prog;
		new->prev = NULL;
		if (*prog)
			(*prog)->prev = new;
		*prog = new;
	}
	return (repeat);
}

t_prog	*prog_clear(t_prog **prog)
{
	t_prog	*temp;

	while (*prog)
	{
		temp = (*prog)->next;
		free(*prog);
		*prog = temp;
	}
	return (NULL);
}

int	prog_disp(t_prog *prog)
{
	static const char	*str_ops[12] = {"sa", "sb", "ss", "pa", "pb", "ra",
										"rb", "rr", "rra", "rrb", "rrr", "Err"};
	static const int	int_ops[11] = {05, 06, 07, 011, 012,
										021, 022, 023, 041, 042, 043};
	int					i;
	int					j;

	while (prog && prog->next)
		prog = prog->next;
	while (prog)
	{
		i = 0;
		while (i < 11 && int_ops[i] != prog->op)
			i++;
		j = 0;
		while (j++ < prog->repeat)
		{
			write(1, str_ops[i], 2 + (i > 7));
			write(1, "\n", 1);
		}
		prog = prog->prev;
	}
	return (0);
}

int	prog_trimra(t_stack *a, t_prog **prog)
{
	t_prog	*temp;
	int		n;

	if (*prog && (*prog)->op == OP_RA)
	{
		temp = *prog;
		n = (*prog)->repeat;
		*prog = (*prog)->next;
		free(temp);
		stack_rotate(a, -n);
		return (-n);
	}
	else
		return (0);
}
