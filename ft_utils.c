/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 17:11:37 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 18:09:22 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

int	ft_atoichk(const char *str, int *n)
{
	unsigned long long int	ret;
	int						sgn;

	sgn = 1;
	if (*str == '-')
		sgn = -1;
	if (*str == '-' || *str == '+')
		str++;
	ret = 0;
	while (*str >= '0' && *str <= '9')
	{
		ret = ret * 10 + *str - '0';
		if (ret > 2147483648 || (ret > 2147483647 && sgn > 0))
			return (-1);
		str++;
	}
	if (*str)
		return (-1);
	*n = sgn * ret;
	return (0);
}

int	min(int a, int b)
{
	if (b < a)
		return (b);
	return (a);
}

int	max(int a, int b)
{
	if (b > a)
		return (b);
	return (a);
}

int	tern(int a, int b, int c)
{
	if (a)
		return (b);
	return (c);
}
