/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 17:00:12 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 18:08:21 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

int	stackwise_op(int op, t_stack *s1, t_stack *s2)
{
	int	var1;
	int	var2;

	if (op & OP_SX && s1->size > 1)
	{
		stack_pull(s1, &var1);
		stack_pull(s1, &var2);
		return (stack_push(s1, var1) + stack_push(s1, var2));
	}
	else if (op & OP_PX && stack_pull(s2, &var1) > 0)
		return (stack_push(s1, var1));
	else if (op & OP_RX)
		return (stack_rotate(s1, 1));
	else if (op & OP_RRX)
		return (stack_rotate(s1, -1));
	return (0);
}

int	stack_op(int op, t_stack *a, t_stack *b)
{
	if (op & OP_XA)
		stackwise_op(op, a, b);
	if (op & OP_XB)
		stackwise_op(op, b, a);
	return (op);
}

int	stack_findval(const t_stack *a, int val)
{
	int	i;

	if (!a)
		return (-1);
	i = 0;
	while (i < a->size)
	{
		if (stack_r(a, i) == val)
			return (i);
		i++;
	}
	return (-1);
}

int	stack_settop_both(t_stack *stack[2], int targ[2], t_prog **prog)
{
	int	single;
	int	join_a;
	int	join_b;

	single = min(targ[0], stack[0]->size - targ[0])
		+ min(targ[1], stack[1]->size - targ[1]);
	join_a = max(targ[0], targ[1]);
	join_b = max(stack[0]->size - targ[0], stack[1]->size - targ[1]);
	if (single < min(join_a, join_b))
		return (stack_settop(stack[0], OP_XA, targ[0], prog)
			+ stack_settop(stack[1], OP_XB, targ[1], prog));
	if (join_a < join_b)
		single = OP_RX;
	else
	{
		single = OP_RRX;
		targ[0] = stack[0]->size - targ[0];
		targ[1] = stack[1]->size - targ[1];
	}
	stack_rotate(stack[0], tern(single == OP_RX, targ[0], -targ[0]));
	stack_rotate(stack[1], tern(single == OP_RX, targ[1], -targ[1]));
	prog_push(prog, single + OP_XX, min(targ[0], targ[1]));
	prog_push(prog, single + tern(targ[0] > targ[1], OP_XA, OP_XB),
		tern(targ[0] > targ[1], targ[0] - targ[1], targ[1] - targ[0]));
	return (max(targ[0], targ[1]));
}

int	stack_settop(t_stack *a, int stack, int targ, t_prog **prog)
{
	int	n;
	int	op;

	if (targ < a->size - targ)
		op = OP_RX;
	else
		op = OP_RRX;
	op += stack;
	n = min(targ, a->size - targ);
	prog_push(prog, op, n);
	stack_rotate(a, 2 * n * !(op & OP_RRX) - n);
	return (n);
}
