/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   purge.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/20 15:32:55 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 18:10:49 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

int	pivot_find(const t_stack *a)
{
	int	i;
	int	score;
	int	pivot;
	int	score_max;

	i = 0;
	pivot = 0;
	score_max = 0;
	while (i < a->size)
	{
		score = pivot_score(a, i);
		if (score > score_max)
		{
			score_max = score;
			pivot = i;
		}
		i++;
	}
	return (pivot);
}

int	pivot_score(const t_stack *a, int pivot)
{
	int	i;
	int	max;
	int	score;

	i = pivot + 1;
	max = stack_r(a, pivot);
	score = 1 + (stack_r(a, pivot) > stack_r(a, i));
	while (i % a->size != pivot)
	{
		if (stack_r(a, i) > max)
		{
			score += 1 + (stack_r(a, i) > stack_r(a, i + 1)
					&& stack_r(a, i + 1) > max);
			max = stack_r(a, i);
		}
		i++;
	}
	return (score);
}

int	stack_purge(t_stack *a, t_stack *b, t_prog **prog)
{
	int	ret;
	int	pivot;
	int	istopurge;
	int	n;

	ret = 0;
	n = a->size;
	pivot = pivot_find(a);
	while (ret < n)
	{
		istopurge = stack_istopurge(a, pivot);
		if (istopurge == 1)
			ret += prog_push(prog, stack_op(OP_PB, a, b), 1);
		else if (istopurge == 2)
		{
			ret += prog_push(prog, stack_op(OP_SA, a, b), 1);
			n++;
			pivot++;
		}
		else
			ret += prog_push(prog, stack_op(OP_RA, a, b), 1);
		if (--pivot < 0)
			pivot += a->size;
	}
	return (n);
}

int	stack_istopurge(const t_stack *a, int i)
{
	int	max;

	if (!i)
		return (2 * (a->ring[a->top] > stack_r(a, 1)));
	max = stack_r(a, i);
	while (++i % a->size)
	{
		if (stack_r(a, i) > max)
			max = stack_r(a, i);
	}
	return ((stack_r(a, i) < max) + 2 * (stack_r(a, i) > stack_r(a, i + 1)
			&& stack_r(a, i + 1) > max));
}
