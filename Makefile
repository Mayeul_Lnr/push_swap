MANDA_SRCS 	=	main.c \
				purge.c \
				merge.c

BONUS_SRCS	=	main_bonus.c \
				gnl_bonus.c \
				src_bonus.c

COMMON_SRCS =	stack1.c \
				stack2.c \
				stack_parsing.c \
				prog.c \
				ft_utils.c \
				ft_split.c

OBJS		= ${MANDA_SRCS:.c=.o} ${COMMON_SRCS:.c=.o}
BONUS_OBJS	= ${BONUS_SRCS:.c=.o} ${COMMON_SRCS:.c=.o}

NAME		= push_swap
BONUS_NAME	= checker
CC			= clang -Wall -Wextra -Werror
RM			= rm -f

${NAME}:	${OBJS}
	${CC} ${OBJS} -o ${NAME}

all:		${NAME}

bonus:		${BONUS_OBJS}
	${CC} ${BONUS_OBJS} -o ${BONUS_NAME}

%.o:		%.c
	${CC} -c $< -o $@

clean:
	${RM} ${OBJS} ${BONUS_OBJS}

fclean:		clean
	${RM} ${NAME} ${BONUS_NAME}

re:			fclean all

.PHONY:		all bonus clean fclean re
