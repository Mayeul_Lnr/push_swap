/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 19:47:47 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/10/07 15:28:36 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

static int	stack_min(t_stack *a)
{
	int	min;
	int	min_i;
	int	i;

	if (!a->size)
		return (-1);
	min = a->ring[a->top];
	min_i = 0;
	i = 0;
	while (++i < a->size)
	{
		if (stack_r(a, i) < min)
		{
			min = stack_r(a, i);
			min_i = i;
		}
	}
	return (min_i);
}

int	main(int ac, char **av)
{
	t_stack	*a;
	t_stack	*b;
	t_prog	*prog;

	if (ac < 2)
		return (0);
	a = stack_parsing(ac, av);
	if (!a || !a->size)
		return (-write(2, "Error\n", 6));
	if (a->size == 3 && a->ring[1] < a->ring[3] && a->ring[3] < a->ring[2])
		return (stack_clear(&a) * write(1, "sa\nra\n", 6));
	b = stack_init(a->max_size);
	if (!b)
		return (stack_clear(&a) - 1);
	prog = NULL;
	stack_purge(a, b, &prog);
	prog_trimra(a, &prog);
	stack_merge(a, b, &prog);
	stack_settop(a, OP_XA, stack_min(a), &prog);
	prog_disp(prog);
	prog_clear(&prog);
	return (stack_clear(&a) + stack_clear(&b));
}
