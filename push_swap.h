/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 19:48:52 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 23:22:02 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <stdlib.h>
# include <unistd.h>

# define OP_XA 01
# define OP_XB 02
# define OP_XX 03

# define OP_SX 04
# define OP_SA 05
# define OP_SB 06
# define OP_SS 07

# define OP_PX 010
# define OP_PA 011
# define OP_PB 012

# define OP_RX 020
# define OP_RA 021
# define OP_RB 022
# define OP_RR 023

# define OP_RRX 040
# define OP_RRA 041
# define OP_RRB 042
# define OP_RRR 043

# define GNL_BUFFER_SIZE 256

typedef struct s_stack
{
	int	*ring;
	int	top;
	int	bot;
	int	size;
	int	max_size;
}	t_stack;

typedef struct s_prog
{
	int				op;
	int				repeat;
	struct s_prog	*next;
	struct s_prog	*prev;
}	t_prog;

/*
**	FUNCTIONS THAT ARE COMMON TO MANDATORY & BONUS PARTS
**
**	Stack management functions: (stack1.c)
*/

int		stack_r(const t_stack *a, int i);

int		stack_rotate(t_stack *a, int direction);

int		stack_push(t_stack *a, int val);

int		stack_pull(t_stack *a, int *val);

int		stack_clear(t_stack **a);

/*
**	More stack management functions: (stack2.c)
*/

int		stackwise_op(int op, t_stack *s1, t_stack *s2);

int		stack_op(int op, t_stack *a, t_stack *b);

int		stack_findval(const t_stack *a, int val);

int		stack_settop_both(t_stack *stack[2], int targ[2], t_prog **prog);

int		stack_settop(t_stack *a, int stack, int target, t_prog **prog);

/*
**	Stack parsing functions (stack_parsing.c)
*/

t_stack	*stack_init(int size);

t_stack	*stack_parsing(int ac, char **av);

/*
**	General prog functions (prog.c)
*/

int		prog_push(t_prog **prog, int op, int repeat);

t_prog	*prog_clear(t_prog **prog);

int		prog_disp(t_prog *prog);

int		prog_trimra(t_stack *a, t_prog **prog);

/*
**	Utilitaries functions: (ft_utils.c)
*/

int		ft_atoichk(const char *str, int *n);

int		min(int a, int b);

int		max(int a, int b);

int		tern(int a, int b, int c);

/*
**	ft_split. Uuuugh.
*/

char	**ft_split(char const *s, char c, int *size);

/*
**	MANDATORY PART ONLY
**
**	This is where we PURGE a stack (purge.c)
*/

int		pivot_find(const t_stack *a);

int		pivot_score(const t_stack *a, int pivot);

int		stack_purge(t_stack *a, t_stack *b, t_prog **prog);

int		stack_istopurge(const t_stack *a, int pivot);

/*
**	This is where we merge b into a (merge.c)
*/

int		stack_target_a(const t_stack *a, int val);

int		stack_target_b(const t_stack *a, t_stack *b);

int		stack_merge(t_stack *a, t_stack *b, t_prog **prog);

/*
**	BONUS PART ONLY
**
**	Prog parsing functions (bonus.c)
*/

int		atoop(const char s[4]);

int		prog_parsing(t_prog **prog);

int		prog_exec(t_prog *prog, t_stack *a, t_stack *b);

int		stack_issorted(t_stack *a);

/*
**	Just a regular get_next_line
*/

int		get_next_line(char **line);

#endif
