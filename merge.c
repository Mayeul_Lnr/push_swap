/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/20 16:41:24 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 18:12:03 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

inline static int	dist(const t_stack *a, int targ)
{
	return (min(targ, a->size - targ));
}

int	stack_target_a(const t_stack *a, int val)
{
	int	i;
	int	val_a;
	int	val_b;

	if (a->size < 2)
		return (0);
	i = 0;
	val_a = stack_r(a, -1);
	val_b = a->ring[a->top];
	while (val > val_b || (val < val_a && val_a < val_b))
	{
		val_a = val_b;
		val_b = stack_r(a, ++i);
	}
	return (i);
}

int	stack_target_b(const t_stack *a, t_stack *b)
{
	int	i;
	int	ret;
	int	score;
	int	score_min;

	score = stack_target_a(a, b->ring[b->top]);
	score_min = dist(a, score);
	ret = 0;
	i = 1;
	while (i < b->size)
	{
		if (dist(b, i) < score_min)
		{
			score = stack_target_a(a, stack_r(b, i));
			score = min(dist(b, i) + dist(a, score), min(max(i, score),
						max(b->size - i, a->size - score)));
			if (score < score_min)
			{
				ret = i;
				score_min = score;
			}
		}
		i++;
	}
	return (ret);
}

int	stack_merge(t_stack *a, t_stack *b, t_prog **prog)
{
	t_stack	*stack[2];
	int		target[2];
	int		ret;

	stack[0] = a;
	stack[1] = b;
	ret = 0;
	while (b->size)
	{
		target[1] = stack_target_b(a, b);
		target[0] = stack_target_a(a, stack_r(b, target[1]));
		ret += stack_settop_both(stack, target, prog);
		ret += prog_push(prog, stack_op(OP_PA, a, b), 1);
	}
	return (ret);
}
