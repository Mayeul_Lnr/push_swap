/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 19:47:47 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/10/07 15:27:57 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

int	main(int ac, char **av)
{
	t_stack	*a;
	t_stack	*b;
	t_prog	*prog;

	if (ac < 2)
		return (0);
	a = stack_parsing(ac, av);
	if (!a || !a->size)
		return (-write(2, "Error\n", 6));
	prog = NULL;
	if (prog_parsing(&prog))
		return (stack_clear(&a) - write(2, "Error\n", 6));
	b = stack_init(a->max_size);
	if (!b)
		return (stack_clear(&a) - 1);
	prog_exec(prog, a, b);
	prog_clear(&prog);
	if (stack_issorted(a) && !b->size)
		write(1, "OK\n", 3);
	else
		write(1, "KO\n", 3);
	return (stack_clear(&a) + stack_clear(&b));
}
