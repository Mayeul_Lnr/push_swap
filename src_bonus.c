/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   src_bonus.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/17 18:05:05 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/29 00:05:21 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

static int	ft_strcmp(const char *s1, const char *s2)
{
	if (!s1 && !s2)
		return (0);
	if (!s1 || !s2)
		return (-1);
	while (*s1 == *s2 && *s1 && *s2)
	{
		s1++;
		s2++;
	}
	return (*s2 - *s1);
}

int	atoop(const char *s)
{
	int					i;
	static const char	*str_ops[11] = {"sa", "sb", "ss", "pa", "pb",
										"ra", "rb", "rr", "rra", "rrb", "rrr"};
	static const int	int_ops[11] = {05, 06, 07, 011, 012,
										021, 022, 023, 041, 042, 043};

	i = -1;
	while (++i < 11)
		if (!ft_strcmp(s, str_ops[i]))
			return (int_ops[i]);
	return (-1);
}

int	prog_parsing(t_prog **prog)
{
	char	*line;
	int		op;
	int		n;

	n = 1;
	while (n)
	{
		n = get_next_line(&line);
		if (n < 0)
			return ((long)prog_clear(prog) + n);
		if (!n && *line)
			n = -1;
		op = atoop(line);
		free(line);
		if (n && (op < 0 || prog_push(prog, op, 1) < 0))
			return ((long)prog_clear(prog) - 1);
	}
	return (0);
}

int	prog_exec(t_prog *prog, t_stack *a, t_stack *b)
{
	int	i;
	int	ret;

	while (prog && prog->next)
		prog = prog->next;
	ret = 0;
	while (prog)
	{
		i = 0;
		while (i++ < prog->repeat)
			stack_op(prog->op, a, b);
		ret += prog->repeat;
		prog = prog->prev;
	}
	return (ret);
}

int	stack_issorted(t_stack *a)
{
	int	i;

	if (a->size < 2)
		return (1);
	i = 0;
	while (i < a->size - 1)
	{
		if (stack_r(a, i) > stack_r(a, i + 1))
			return (0);
		i++;
	}
	return (1);
}
