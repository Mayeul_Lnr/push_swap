/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/16 19:09:07 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/28 23:23:34 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

int	stack_r(const t_stack *a, int i)
{
	if (!a->size)
		return (0);
	while (i < 0)
		i += a->size;
	return (a->ring[(a->top + i % a->size) % a->max_size]);
}

int	stack_rotate(t_stack *a, int n)
{
	int	i;

	if (a->size < 2)
		return (0);
	n %= a->size;
	i = 0;
	if (n < 0)
	{
		while (i-- > n)
		{
			a->bot = (a->bot - 1 + a->max_size) % a->max_size;
			a->top = (a->top - 1 + a->max_size) % a->max_size;
			a->ring[a->top] = a->ring[a->bot];
		}
	}
	else
	{
		while (i++ < n)
		{
			a->ring[a->bot] = a->ring[a->top];
			a->bot = (a->bot + 1) % a->max_size;
			a->top = (a->top + 1) % a->max_size;
		}
	}
	return (n);
}

int	stack_push(t_stack *a, int val)
{
	if (a->size >= a->max_size - 1)
		return (-1);
	if (--a->top < 0)
		a->top += a->max_size;
	a->size++;
	a->ring[a->top] = val;
	return (0);
}

int	stack_pull(t_stack *a, int *val)
{
	if (!a || !a->size)
		return (0);
	if (!val)
		return (-1);
	*val = a->ring[a->top];
	a->size--;
	a->top = (a->top + 1) % a->max_size;
	return (1);
}

int	stack_clear(t_stack **a)
{
	if (!a || !*a)
		return (0);
	free((*a)->ring);
	free(*a);
	return (0);
}
