/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/17 14:16:52 by mlaneyri          #+#    #+#             */
/*   Updated: 2021/09/24 17:14:14 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./push_swap.h"

static int	ft_splitsize(char const *s, char c)
{
	int	ret;

	ret = 0;
	while (*s)
	{
		while (*s && *s == c)
			s++;
		if (*s)
			ret++;
		while (*s && *s != c)
			s++;
	}
	return (ret);
}

static int	ft_splitfill(char **ret, char const *s, char c)
{
	int		i;
	int		k;

	k = 0;
	while (*s)
	{
		while (*s && *s == c)
			s++;
		i = 0;
		while (s[i] && s[i] != c)
			i++;
		if (!i)
			return (0);
		ret[k] = malloc(i + 1);
		if (!ret[k])
			return (k);
		i = 0;
		while (*s && *s != c)
		{
			ret[k][i++] = *s;
			s++;
		}
		ret[k++][i] = 0;
	}
	return (0);
}

char	**ft_split(char const *s, char c, int *size)
{
	char	**ret;
	int		fuck;

	*size = ft_splitsize(s, c);
	ret = malloc(sizeof(char *) * (*size + 1));
	if (!ret)
		return (NULL);
	fuck = ft_splitfill(ret, s, c);
	if (fuck)
	{
		while (--fuck)
			free(ret[fuck]);
		free(ret);
		return (NULL);
	}
	ret[*size] = 0;
	return (ret);
}
